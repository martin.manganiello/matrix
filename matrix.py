from copy import deepcopy
import functools as ft
import operator


class Matrix(object):
    """ A Python matrix class with basic operations and operator overloading.\n
    - **parameters**, **types**
    :param: listoflists: A vector that contains the rows and columns of the matrix\n
    :type: listoflists: list
    m = rows
    n = columns
    """
    def __init__(self, listoflists):
        if isinstance(listoflists, list):
            self.__values = listoflists[:]
            self.__columns = len(listoflists[0])
            self.__rows = len(listoflists)
        else:
            raise TypeError("{} has send but must be a list of lists".format(type(listoflists)))

    def __add__(self, mat):
        """ Add a matrix to this matrix and
        return the new matrix.
        """
        if self.getRank() != mat.getRank():
            raise ValueError("Trying to add matrixes of varying rank!")
        listoflist = [[sum(item) for item in zip(self.values[x], mat[x])] for x in range(self.rows)]
        return Matrix(listoflist)

    def scalar_product(self, scalar):
        ''' Return the product of a scalar by a matrix'''
        scalar = [[n*scalar for n in row] for row in self.values]
        return Matrix(scalar)

    def __mul__(self, mat):
        """ Multiple a matrix with this matrix and
        return the new matrix.
        """
        if self.columns != mat.rows:
            raise ValueError("Matrices cannot be multipled!")
        mult = [[sum([ai*bi for ai, bi in zip(row_a, col_b)])
                   for col_b in zip(*mat.values)] for row_a in self.values]
        return Matrix(mult)

    def transpose(self):
        """ Return a transpose of the matrix """
        res = [list(item) for item in zip(*self.values)]
        return Matrix(res)

    def determinant(self):
        if self.rows != self.columns:
            raise ValueError("Matrix must be square")
        n = self.columns
        AM = deepcopy(self.values)
        for fd in range(n):
            for i in range(fd+1,n):
                if AM[fd][fd] == 0:
                    AM[fd][fd] == 1.0e-18
                crScaler = AM[i][fd] / AM[fd][fd] 
                for j in range(n): 
                    AM[i][j] = AM[i][j] - crScaler * AM[fd][j]
        product = 1.0
        for i in range(n):
            product *= AM[i][i]
        return round(product)

    def cofactor_matrix(self):
        cofactors = []
        for r in range(self.rows):
            cofactorRow = []
            for c in range(self.rows):
                minor = [row[:c] + row[c+1:] for row in (self[:r] + self[r+1:])]
                cofactorRow.append(((-1)**(r+c)) * Matrix(minor).determinant())
            cofactors.append(cofactorRow)
        return Matrix(cofactors)

    def inverted(self):
        determinant = self.determinant()
        if self.rows == 2:
            return [[self[1][1]/determinant, -1*self[0][1]/determinant],
                    [-1*self[1][0]/determinant, self[0][0]/determinant]]
        cofactors = list(map(list, zip(*self.cofactor_matrix().values)))
        for r in range(len(cofactors)):
            for c in range(len(cofactors)):
                cofactors[r][c] = cofactors[r][c]/determinant
        return Matrix(cofactors)

    def get_rows(self, rows):
        return [[n for n in row] for row in self.values for fila in rows if
                  self.values[(fila-1)] == row]

    def get_columns(self, columns):
        return [list(map(lambda e: e[col-1], self.values)) for col in columns]

    def since_column(self, column):
        return [i[column-1:] for i in self.values]

    def shape(self, flat, dims):
        """ Take a flat vector and convert it into a matrix using recursion """
        subdims = dims[1:]
        subsize = ft.reduce(operator.mul, subdims, 1)
        if dims[0] * subsize != len(flat):
            raise ValueError("Size does not match or invalid")
        if not subdims:
            return flat
        return [self.shape(flat[i:i+subsize], subdims)
                for i in range(0, len(flat), subsize)]

    def __eq__(self, mat):
        return mat.values == self.values

    def getRank(self):
        return self.rows, self.columns

    def __getitem__(self, f):
        return self.values[f]

    def __setitem__(self, f, item):
        self.values[f] = item

    @property
    def rows(self):
        return self.__rows

    @property
    def columns(self):
        return self.__columns

    @property
    def values(self):
        return self.__values
    
    def __str__(self):
        s = '\n'.join(['\t'.join([str(item) for item in row])
                      for row in self.values])
        return s + '\n'

    def __repr__(self):
        return "Matrix: {}, rank: {}".format(str(self.values), str(self.getRank()))
