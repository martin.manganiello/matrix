import pytest
from matrix import Matrix


MATRIX = [
    [1, 2, 3, 4, 5, 6],
    [10, 11, 12, 13, 14, 15],
    [-1, -2, -3, -4, -5, -6]
]


def test_addition():
    """Matrices addition"""
    matrix1 = Matrix(MATRIX)
    matrix2 = Matrix(MATRIX)
    result_expected = [
        [2, 4, 6, 8, 10, 12],
        [20, 22, 24, 26, 28, 30],
        [-2, -4, -6, -8, -10, -12]
    ]
    res = matrix1 + matrix2
    assert res == Matrix(result_expected)


def test_scalar_product():
    """Scalar multiplication"""
    scalar = 2
    result_expected = [
        [2, 4, 6, 8, 10, 12],
        [20, 22, 24, 26, 28, 30],
        [-2, -4, -6, -8, -10, -12]
    ]
    res = Matrix(MATRIX).scalar_product(scalar)
    assert res == Matrix(result_expected)


def test_product():
    """ Matrices multiplication """
    A = [[1,2], [3,4]]
    B = [[5,6], [0,7]]
    result_expected = [[5,20], [15,46]]
    res = Matrix(A) * Matrix(B)
    assert res == Matrix(result_expected)


def test_transpose():
    """ Matrix transpose """
    res = Matrix(MATRIX).transpose()
    result_expected = [
        [1, 10, -1],
        [2, 11, -2],
        [3, 12, -3],
        [4, 13, -4],
        [5, 14, -5],
        [6, 15, -6]
    ]
    assert res == Matrix(result_expected)


def test_determinant():
    """ Matrix determinant """
    matrix = [[3, -1, -1], [2, 1, 0], [3, 1, 2]]
    assert Matrix(matrix).determinant() == 11


def test_adjugate():
    """ Adjugate matrix """
    matrix = [[-3, 2, 0], [1,-1,2], [-2,1,3]]
    result_expected = [[-5, -7, -1], [-6, -9, -1], [4, 6, 1]]
    res = Matrix(matrix).cofactor_matrix()
    assert res == Matrix(result_expected)


def test_inverted():
    """ Inverted matrix """
    matrix = [[2, 1, 3], [-1, 2, 4], [0, 1, 3]]
    result_expected = [[0.5, 0.0, -0.5], [0.75, 1.5, -2.75], [-0.25, -0.5, 1.25]]
    res = Matrix(matrix).inverted()
    assert res == Matrix(result_expected)

# Additional methods

def test_get_rows():
    matrix = [
        [3, -1, -1],
        [2, 1, 0],
        [3, 1, 2]
    ]
    rows = [1, 3]
    res = Matrix(matrix).get_rows(rows)
    result_expected = [[3, -1, -1], [3, 1, 2]]
    assert result_expected == res


def test_get_columns():
    matrix = [[3, -1, -1], [2, 1, 0], [3, 1, 2]]
    columns = [1, 3]
    res = Matrix(matrix).get_columns(columns)
    result_expected = [[3, 2, 3], [-1, 0, 2]]
    assert result_expected == res


def test_get_matrix_since_column():
    matrix = Matrix(MATRIX)
    since_column = 5
    result_expected = [[5, 6], [14, 15], [-5, -6]]
    res = matrix.since_column(since_column)
    assert res == result_expected


def test_show():
    print(Matrix(MATRIX))
